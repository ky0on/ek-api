#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import warnings
from easydict import EasyDict


def translate_column(column):

    dictionary = EasyDict({
        'air': {'ja': '気温', 'en': 'Air temperature', 'unit': 'C'},
        'humid': {'ja': '相対湿度', 'en': 'Relative humidity', 'unit': '%'},
        'rad': {'ja': '日射照度', 'en': 'Irradiance', 'unit': 'W/m<sup>2</sup>'},
        'vwc': {'ja': '土壌体積含水率', 'en': 'Soil VWC', 'unit': '%'},
        'ec': {'ja': '土壌EC', 'en': 'Soil EC', 'unit': 'mS/cm'},
        'mj': {'ja': '日射量', 'en': 'Irradiance', 'unit': 'MJ'},
        'hd': {'ja': '飽差', 'en': 'Humidity Deficit', 'unit': 'g/m<sup>3</sup>'},
        'dew': {'ja': '露点温度', 'en': 'Dew temperature', 'unit': 'C'},
        'abshumid': {'ja': '絶対湿度', 'en': 'Absolute humidity', 'unit': 'g/m<sup>3</sup>'},
        'ta1': {'ja': '温度A1', 'en': 'Temperature A1', 'unit': 'C'},
        'ta2': {'ja': '温度A2', 'en': 'Temperature A2', 'unit': 'C'},
        'tb1': {'ja': '温度B1', 'en': 'Temperature B1', 'unit': 'C'},
        'tb2': {'ja': '温度B2', 'en': 'Temperature B2', 'unit': 'C'},
        'ppm': {'ja': 'CO<sub>2</sub>濃度', 'en': 'CO<sub>2</sub>', 'unit': 'ppm'},
    })

    if column not in dictionary.keys():
        warnings.warn(f'{column} is not in dictionary.')
        return {'ja': '', 'en': '', 'unit': ''}
    else:
        return dictionary[column]


if __name__ == '__main__':
    print(translate_column('air'))
