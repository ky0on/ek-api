#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pandas as pd
import numpy as np
import requests


class api():

    def __init__(self, id=None, password=None):
        ''' Get Token '''

        if id and password:
            url = 'https://api.e-kakashi.com/api/v1/auth'
            data = {'id': id, 'password': password}
            response = requests.post(
                url=url,
                data=data,
            )
            token = response.json()['token']
            self.token = token
        else:
            self.token = None
            print('No token is set.')

    def load_csv(self, path, new_column_names=False, error_values=(999.99, 9999.99)):
        ''' Load local csv downloaded via WebApp '''

        df = pd.read_csv(
            path, comment='#', header=0,
            names=('snid', 'datetime', 'air', 'humid', 'vwc', 'ec', 'rad', 'ppm',
                   'ta1', 'ta2', 'tb1', 'tb2', 'hd', 'dew', 'abshumid'),
            parse_dates=['datetime'], index_col='datetime',
        )

        #replace error values
        for v in error_values:
            df.replace(v, np.nan, inplace=True)

        #generate MJ
        df['mj'] = df['rad'] * 60 * 10 / 10**6

        if new_column_names:
            df.rename(columns={
                'air': 'temperature',
                'humid': 'humidity',
                'abshumid': 'absoluteHumidity',
                'dew': 'dewPoint',
                'hd': 'humidityDeficit',
                'vwc': 'soilVwc',
                'rad': 'solarIrradiance',
                'ta1': 'thermistor1',
                'ta2': 'thermistor2',
                'tb1': 'thermistor3',
                'tb2': 'thermistor4',
                'ppm': 'co2',
            }, inplace=True)

        return df

    def load_imdc_csv(self, path, timezone='Asia/Tokyo'):
        ''' Load local csv downloaded from IMDC '''

        imdc = pd.read_csv(path, index_col=False)
        imdc.set_index('MEASURE_DATETIME', inplace=True)
        imdc.index = pd.to_datetime(imdc.index).tz_localize(timezone)
        return imdc

    def get_snlist(self, id_only=False):
        ''' Get list of sensor nodes '''

        params = {'token': self.token}
        url = 'https://api.e-kakashi.com/api/v1/sn'
        response = requests.get(
            url=url,
            params=params,
        )

        if id_only:
            return [sn['id'] for sn in response.json()['sns']]
        else:
            return response.json()

    def get_measure(self, snids, fr, to, timezone='Asia/Tokyo', with_corrected=False):
        ''' Get measure data via ekapi  '''

        if not isinstance(snids, list):
            raise Exception('snids must be list.')

        fr = pd.to_datetime(fr)
        fr_utc = fr.tz_localize(timezone).tz_convert('UTC')
        to = pd.to_datetime(to)
        to_utc = to.tz_localize(timezone).tz_convert('UTC')

        url = 'https://api.e-kakashi.com/api/v1/measure'

        response = requests.get(
            url=url,
            params={
                'token': self.token,
                'sn': ','.join(snids),
                'from': fr_utc.strftime('%Y-%m-%d %H:%M:%S'),
                'to': to_utc.strftime('%Y-%m-%d %H:%M:%S'),
            }
        )

        #error
        if not response.ok:
            raise Exception('API request error.')
        elif 'measures' not in response.json().keys():
            raise Exception('No measure data in response.')

        data = []
        for i, measure in enumerate(response.json()['measures']):
            snid = measure['sn']
            df = pd.DataFrame.from_dict(measure['measure'], dtype=float)

            if df.shape[0] == 0:
                print('No data was found for', snid)
                continue

            df.set_index('datetime', inplace=True)
            df.index = pd.to_datetime(df.index)
            df.index = df.index.tz_localize('UTC').tz_convert(timezone).tz_localize(None)
            df['snid'] = snid
            df['mj'] = df['solarIrradiance'] * 60 * 10 / 10**6
            df.sort_index(inplace=True)

            data.append(df)

        df = pd.concat(data)
        df.sort_index(inplace=True)

        #remove corrected data
        if not with_corrected:
            df = df[df.columns.drop(list(df.filter(regex='corrected')))]

        return df


if __name__ == '__main__':
    import os

    id = os.environ['API_ID']
    password = os.environ['API_PASS']
    api = api(id, password)

    # snlist = api.get_snlist()
    # print(snlist)

    # df = api.get_measure(snids=['M03100000278', 'M03100000271'], fr='2018/11/30 00:00:00', to='2018/12/02 23:59:59')
    # print(df)
    # print(df.groupby('snid').temperature.count())

    csv = api.load_csv('measure.csv', new_column_names=True)
    print(csv)
    print(csv.columns)

    # imdc = api.load_imdc_csv('imdc.csv')
    # print(imdc)
