#!usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='ekapi',
    packages=find_packages(),
    version='v3.1',
    description="e-kakashi api",
    author='kyon',
    url='https://ky0on@bitbucket.org/ky0on/ek-api.git',
    classifiers=[],
)
